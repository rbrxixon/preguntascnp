const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        title_answer: { type: DataTypes.STRING, allowNull: true },
        option_value: { type: DataTypes.BOOLEAN, allowNull: true } 
    };

    const options = {
        // disable default timestamp fields (createdAt and updatedAt)
        timestamps: false     
    };

    return sequelize.define('answer', attributes, options);
}