const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        title: { type: DataTypes.STRING, allowNull: false },
        category: { type: DataTypes.INTEGER, allowNull: false },
        subject: { type: DataTypes.INTEGER, allowNull: false },
        created: { type: DataTypes.DATE, allowNull: false, defaultValue: DataTypes.NOW },
        edited: { type: DataTypes.DATE },
        verified: { type: DataTypes.DATE },
        deleted: { type: DataTypes.DATE },
        isVerified: {
            type: DataTypes.VIRTUAL,
            get() { return !!(this.verified); }
        },
        isActive: {
            type: DataTypes.VIRTUAL,
            get() { return !!(this.deleted); }
        }
    };

    const options = {
        // disable default timestamp fields (createdAt and updatedAt)
        timestamps: false     
    };

    return sequelize.define('question', attributes, options);
}