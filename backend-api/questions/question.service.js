﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const crypto = require("crypto");
const { Op } = require('sequelize');
const db = require('_helpers/db');
const Role = require('_helpers/role');

module.exports = {
    create,
    edit,
    getAll,
    getById,
    delete: _delete
};

async function create(q, accountId) {
    // create question object
    const question = new db.Question(q);
    question.accountId = accountId; 

    // save question
    await question.save();
}

async function createAnswers(a) {
    console.log(a);
    // create question object
    //const question = new db.Question(q);
    //question.accountId = accountId; 

    // save question
    //await question.save();
}

async function edit(id, q) {
    const question = await getQuestion(id);

    // copy params to question and save
    Object.assign(question, q);
    question.edited = Date.now();
    await question.save();

    return basicDetails(question);
}

async function create2(params, accountId) {
    // create question object
    const question = new db.Question(params);
    question.accountId = accountId;

    // save question
    //await question.save();
}

async function edit2(id, params) {
    const question = await getQuestion(id);

    // copy params to question and save
    Object.assign(question, params);
    question.edited = Date.now();
    await question.save();

    return basicDetails(question);
}

async function getAll() {  
    const questions = await db.Question.findAll();
    return questions.map(x => basicDetails(x));
}

async function getAllSimple() {  
    const questions = await db.Question.findAll(id, {
        include: [{
            model: db.Answer,
            attributes: ['title_answer','option_value'] }]
        });
    return questions.map(x => basicDetails(x));
}

async function getById(id) {
    const question = await getQuestion(id);
    return basicDetails(question);
}

async function getByIdSimple(id) {
    const question = await getQuestion(id);
    return moreBasicDetails(question);
}


async function _delete(id) {
    const question = await getQuestion(id);
    await question.destroy();
}

// helper functions

async function getQuestion(id) {
    const question = await db.Question.findByPk(id);
    if (!question) throw 'Question not found';
    return question;
}

async function getQuestionSimple(id) {
    //const question = await db.Question.findByPk(id);
    const question = await db.Question.findByPk(id, {
         include: [{
             model: db.Answer,
             attributes: ['title_answer','option_value'] }]
         });
    if (!question) throw 'Question not found';
    return question;
}

function basicDetails(question) {
    const { id, title, category, created, edited, verified, deleted, updated, isVerified, isActive, accountId, answers} = question;
    return { id, title, category, created, edited, verified, deleted, updated, isVerified, isActive, accountId, answers };
}

function moreBasicDetails(question) {
    const { id, title, category, answers} = question;
    return { id, title, category, answers };
}
