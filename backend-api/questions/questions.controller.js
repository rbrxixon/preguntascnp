﻿const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const Role = require('_helpers/role');
const questionService = require('./question.service');

// routes
router.post('/create', authorize(), createSchema, create); 
router.post('/createAnswers', authorize(), createAnswers); 
router.get('/', authorize(), getAll);
router.get('/', authorize(), getAll);
router.get('/:id', authorize(), getById);
router.post('/', authorize(), createSchema, create);
router.put('/:id', authorize(), editSchema, edit);
router.delete('/:id', authorize(), _delete);


module.exports = router;

function createSchema(req, res, next) {
    console.log(req.body);
    const schema = Joi.object({
        category: Joi.number().required(),
        subject: Joi.number().required(),
        title: Joi.string().min(6).required(),
        answers: Joi.array()
        .items({
            title_answer: Joi.string().min(1).required()
        })
    });

//option_value: Joi.boolean().required()

    //acceptTerms: Joi.boolean().valid(true).required()
    validateRequest(req, next, schema);
}

function editSchema(req, res, next) {
    const schemaRules = {
        title: Joi.string().min(6).empty('')
    };
    const schema = Joi.object(schemaRules);
    validateRequest(req, next, schema);
}

function getAll(req, res, next) {
    questionService.getAll()
        .then(questions => res.json(questions))
        .catch(next);
}

function getById(req, res, next) {
    questionService.getById(req.params.id)
        .then(question => question ? res.json(question) : res.sendStatus(404))
        .catch(next);
}

function create(req, res, next) {
    //console.log(req);
    questionService.create(req.body, req.user.id)
        .then(question => res.json(question))
        .catch(next);
}

function createAnswers(req, res, next) {
    console.log(req);
    questionService.createAnswers(req.body)
        .then(answers => res.json(answers))
        .catch(next);
}

function edit(req, res, next) {
    questionService.edit(req.params.id, req.body)
        .then(question => res.json(question))
        .catch(next);
}

function _delete(req, res, next) {       
    /*
    // users can delete their own account and admins can delete any account    
    if (question.accountId !== req.user.id && req.user.role !== Role.Admin) {
        return res.json({ message: 'Unauthorized' });
    }
    */
    questionService.delete(req.params.id)    
        .then(() => res.json({ message: 'Question deleted successfully' }))
        .catch(next);
}
