import { BehaviorSubject } from 'rxjs';

import config from 'config';
import { fetchWrapper } from '@/_helpers';

const baseUrl = `${config.apiUrl}/questions`;

export const questionService = {
    create,
    createAnswers,
    getAll,
    getById,
    edit,
    delete: _delete
};

function getAll() {
    return fetchWrapper.get(baseUrl);
}

function getById(id) {
    return fetchWrapper.get(`${baseUrl}/${id}`);
}

function create(params) {
    //console.log(params);
    return fetchWrapper.post(baseUrl, params);
}

function createAnswers(params) {
    return fetchWrapper.post(baseUrl, params);
}

function edit(id, params) {
    return fetchWrapper.put(`${baseUrl}/${id}`, params)
        .then(question => {
            return question;
        });
}

// prefixed with underscore because 'delete' is a reserved word in javascript
function _delete(id) {
    return fetchWrapper.delete(`${baseUrl}/${id}`)
        .then(x => {
            return x;
        });
}
