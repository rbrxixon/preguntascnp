import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage, FieldArray, yupToFormErrors, validateYupSchema } from 'formik';
import * as Yup from 'yup';

import { Question, questionService, alertService } from '@/_services';

function AddEdit({ history, match }) {
    const { id } = match.params;
    const isAddMode = !id;
    
    const initialValues = {
        category: '1',
        subject: '1',
        title: '',
        answers: [
            {
                title_answer: '',
                option_value: false
            },
            {
                title_answer: '',
                option_value: false
            },
            {
                title_answer: '',
                option_value: false
            }
        ]
    };

            
/*
    Yup.addMethod( Yup.array, 'atMostOne', function( args ) {
        const { message, predicate } = args
        return this.test( 'atMostOne', message, function( list ) {     
            const listFiltered = list.filter( predicate )
             
            console.log("Marcadas " + list.filter( predicate ).length )
            console.log(list);
            //console.log(listFiltered)
            

            for (let i = 0; i < list.length; i++) {
                const element = list[i];
                console.log("*** ENTRA ****") ; 
                console.log("[i] = " + i + " : " + element.option_value)
                if (element.option_value === true) {
                    console.log("TRUE");
                    return true
            } else {
                console.log("FALSE");
                const s  = this.createError({
                    path: `answers[${i}].option_value`,
                    message: message,
              });
               return s 
            }
                


            if (listFiltered.length === 1) {
                return true
            } else {
                return this.createError({
                    path: `answers[0].option_value`,
                    message: message,
              });
              
        }
            
        })
      })
*/

    const ANSWER_SCHEMA = Yup.object().shape({
        title_answer: Yup
            .string()
            .required('Answer is required')
            .min(2, 'Answer must be at least 2 characters'),
        option_value: Yup
            .array()
            .min(1, "at least one must be selected")
      })
/*
      const validationSchema = Yup.object().shape({
        title: Yup
            .string()
            .required('Title is required')
            .min(6, 'Title must be at least 6 characters'),
        answers: Yup
            .array()
            .min(1, "at least one must be selected")
        })
*/

const validationSchema = Yup.object().shape({
    title: Yup
        .string()
        .required('Title is required')
        .min(6, 'Title must be at least 6 characters'),
    answers: Yup.array()
        .of( ANSWER_SCHEMA )
});

/*
    const validationSchema = Yup.object().shape({
        title: Yup
            .string()
            .required('Title is required')
            .min(6, 'Title must be at least 6 characters'),
        answers: Yup.array()
        .of( EMAIL_SCHEMA )
        .ensure()
        .atMostOne({ 
            message: 'Only 1 option is required',
            predicate: an => an.option_value === true
          })
          .strict()
    });
  */  
    
     function onSubmit(fields, { setStatus, setSubmitting }) {
        setStatus();
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(fields, null, 4))
        if (isAddMode) {
            createQuestion(fields, setSubmitting);
        } else {
            editQuestion(id, question, setSubmitting);
        }
    }

    function createQuestion(fields, setSubmitting) {
        console.log(fields);
        questionService.create(fields)
        //.then(() => {
        //    questionService.createAnswers(question.answers)
        //    history.push('.');
        //})
            .then(() => {
                alertService.success('Question added successfully', { keepAfterRouteChange: true });
                history.push('.');
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    function editQuestion(id, question, setSubmitting) {
        questionService.edit(id, question)
            .then(() => {
                alertService.success('Update successful', { keepAfterRouteChange: true });
                history.push('..');
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    function createQuestion2(fields, setSubmitting) {
        questionService.create(fields)
            .then(() => {
                alertService.success('Question added successfully', { keepAfterRouteChange: true });
                history.push('.');
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    function editQuestion2(id, fields, setSubmitting) {
        questionService.edit(id, fields)
            .then(() => {
                alertService.success('Update successful', { keepAfterRouteChange: true });
                history.push('..');
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    return (
        <Formik 
            initialValues={initialValues} 
            validationSchema={validationSchema} 
            onSubmit={onSubmit}
            >
            {({ errors, touched, isSubmitting, setFieldValue }) => {
                useEffect(() => {
                    if (!isAddMode) {
                        // get question and set form fields
                        questionService.getById(id).then(question => {
                            const fields = ['category', 'subject', 'title', 'answers'];
                            fields.forEach(field => setFieldValue(field, question[field], false));
                        });
                    }
                }, []);

                return (
                    <Form>
                        <h1>{isAddMode ? 'Create Question' : 'Edit Question'}</h1>
                        <div className="form-row">
                            <div className="form-group col">
                                <label>Category</label>
                                <Field name="category" as="select" className={'form-control' + (errors.category && touched.category ? ' is-invalid' : '')}>
                                    <option value="1">Access</option>
                                    <option value="2">Superior</option>
                                </Field>
                                <ErrorMessage name="category" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group col-5">
                                <label>Subject</label>
                                <Field name="subject" as="select" className={'form-control' + (errors.subject && touched.subject ? ' is-invalid' : '')}>
                                    <option value="1">Constitucion</option>
                                    <option value="2">Drojas</option>
                                </Field>
                                <ErrorMessage name="subject" component="div" className="invalid-feedback" />
                            </div>
                        </div>
                        
                            <div className="form-row">
                                <div className="form-group col">
                                    <label>Title</label>
                                    <Field name="title" type="text" className={'form-control' + (errors.title && touched.title ? ' is-invalid' : '')} />
                                    <ErrorMessage name="title" component="div" className="invalid-feedback" />
                                </div>
                            </div>
                        
                        <FieldArray name="answers" render={arrayHelpers => (
                            <div>
                                <div className="form-row">
                                <div className="form-group col-10"></div>
                                <div className="form-group col-1">RESPUESTA CORRECTA</div>
                                </div>
                                
                            {initialValues.answers.map((answer, index) => (
                                
                                <div key={index}>
                                    <div className="form-row">
                                        <div className="form-group col-10">
                                            <label>Pregunta #{index+1}</label>
                                            <Field name={`answers.[${index}].title_answer`} type="text" className={'form-control' + (errors.answers && touched.answers ? ' is-invalid' : '')} />
                                            <ErrorMessage name={`answers.[${index}].title_answer`} component="div" className="invalid-feedback" />
                                        </div>
                                        <div className="form-group col">
                                            <Field type="checkbox" name={`answers.[${index}].option_value`} className={'form-control' + (errors.answers && touched.answers ? ' is-invalid' : '')} />
                                            <ErrorMessage name={`answers.[${index}].option_value`} component="div" className="invalid-feedback" />
                                        </div>
                                    </div>
                                </div>   

                                ))}
                                </div>
                                )} />
                            
        
                        <div className="form-group">
                            <button type="submit" disabled={isSubmitting} className="btn btn-primary">
                                {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                Save
                            </button>
                            <Link to={isAddMode ? '.' : '..'} className="btn btn-link">Cancel</Link>
                        </div>
                        
 
                    </Form>
                );
            }}
        
        </Formik>
    );
}


export { AddEdit };