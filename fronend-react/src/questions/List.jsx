import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { questionService } from '@/_services';

function List({ match }) {
    const { path } = match;
    const [questions, setQuestions] = useState([]);

     useEffect(() => {
        questionService.getAll().then(x => setQuestions(x));
    }, []);

    function deleteQuestion(id) {
        setQuestions(questions.map(x => {
            if (x.id === id) { x.isDeleting = true; }
            return x;
        }));
        questionService.delete(id).then(() => {
            setQuestions(questions => questions.filter(x => x.id !== id));
        });
    }
    
    return (
        <div>
            <h1>Questions</h1>
            <p>List of questions:</p>
            <Link to={`${path}/create`} className="btn btn-sm btn-success mb-2">Create Question</Link>
    
            {questions.length > 0 
            ?
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th style={{ width: '40%' }}>Title</th>
                            <th style={{ width: '10%' }}>Type</th>
                            <th style={{ width: '20%' }}>Created</th>
                            <th style={{ width: '20%' }}>Author</th>
                            <th style={{ width: '10%' }}>Actions</th>
                        </tr>
                    </thead>
                
                    <tbody>
                        {questions && questions.map(question =>
                            <tr key={question.id}>
                                <td>{question.title}</td>
                                <td>{question.type}</td>
                                <td>{question.created}</td>
                                <td>{question.accountId}</td>
                                <td style={{ whiteSpace: 'nowrap' }}>
                                    <Link to={`${path}/edit/${question.id}`} className="btn btn-sm btn-primary mr-1">Edit</Link>
                                    <button onClick={() => deleteQuestion(question.id)} className="btn btn-sm btn-danger" style={{ width: '60px' }} disabled={question.isDeleting}>
                                        {question.isDeleting 
                                            ? <span className="spinner-border spinner-border-sm"></span>
                                            : <span>Delete</span>
                                        }
                                    </button>
                                </td>
                            </tr>
                        )}
                        {!questions &&
                            <tr>
                                <td colSpan="4" className="text-center">
                                    <span className="spinner-border spinner-border-lg align-center"></span>
                                </td>
                            </tr>
                        }
                    </tbody>
                </table>
            : 
                <h2>No questions created</h2>
            }
        </div>
    );
}

export { List };